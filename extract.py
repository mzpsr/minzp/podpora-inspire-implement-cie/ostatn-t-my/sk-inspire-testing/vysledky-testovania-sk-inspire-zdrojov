import pprint
import io
from owslib.csw import CatalogueServiceWeb
from owslib.fes import PropertyIsEqualTo
import lxml.etree as ET


_FILE_IDENTIFIERS = ["https://data.gov.sk/set/rpi/gmd/42181810/4E93317B0429510DE0530210000A9EA5","https://data.gov.sk/set/rpi/gmd/42181810/4E88A0888B2C5F25E0530210000A63DC","https://data.gov.sk/set/rpi/gmd/42181810/4E93C2A5AEDB5B19E0530110000ACE34","https://data.gov.sk/set/rpi/gmd/42181810/4E93C2A5AEDF5B19E0530110000ACE34","https://data.gov.sk/set/rpi/gmd/42181810/501D7155C7C9645EE0530210000A6A70","https://data.gov.sk/set/rpi/gmd/42181810/4F149A4239964018E0530210000A1948","https://data.gov.sk/set/rpi/gmd/42181810/4F19E731C80E0BB2E0530110000AF35A","https://data.gov.sk/set/rpi/gmd/42181810/4F19E731C80F0BB2E0530110000AF35A","https://data.gov.sk/set/rpi/gmd/42181810/4F1D29041153596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/86D67B11B12424C7E0530110000AA2C2","https://data.gov.sk/set/rpi/gmd/42181810/8A2DDE0C2226184FE0530210000A1F93","https://data.gov.sk/set/rpi/gmd/42181810/876A4078B8561AB3E0530210000A12ED","https://data.gov.sk/set/rpi/gmd/42181810/4F1D2904115C596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/8A4BB67CB1AE5026E0530210000A0CA8","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329402CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329362CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F21D34AE8FE7BA0E0530110000A5453","https://data.gov.sk/set/rpi/gmd/42181810/4F1D2904115D596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/8A671134A5BE737AE0530210000A3952","https://data.gov.sk/set/rpi/gmd/42181810/8A67CB67A019440BE0530210000AAE45","https://data.gov.sk/set/rpi/gmd/42181810/8A67CB67A01A440BE0530210000AAE45","https://data.gov.sk/set/rpi/gmd/42181810/4F1D2904115F596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/4F50F8FB18D23297E0530210000AD7D6","https://data.gov.sk/set/rpi/gmd/42181810/6A344EA44BBE1D87E0530110000A54AA","https://data.gov.sk/set/rpi/gmd/42181810/4F1D29041160596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/8EE23D84EE287198E0530210000ACB6F","https://data.gov.sk/set/rpi/gmd/42181810/8A67CB67A01B440BE0530210000AAE45","https://data.gov.sk/set/rpi/gmd/42181810/8EE23D84EE2D7198E0530210000ACB6F","https://data.gov.sk/set/rpi/gmd/42181810/4F1D29041161596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/8A67CB67A01C440BE0530210000AAE45","https://data.gov.sk/set/rpi/gmd/42181810/8A67CB67A01D440BE0530210000AAE45","https://data.gov.sk/set/rpi/gmd/42181810/4F1D29041162596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/8A67CB67A01E440BE0530210000AAE45","https://data.gov.sk/set/rpi/gmd/42181810/8A67CB67A01F440BE0530210000AAE45","https://data.gov.sk/set/rpi/gmd/42181810/4F1D29041163596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/4F1D29041165596EE0530210000A8020","https://data.gov.sk/set/rpi/gmd/42181810/4F21D34AE8FD7BA0E0530110000A5453","https://data.gov.sk/set/rpi/gmd/42181810/8A6A6700239C5F40E0530210000A893A","https://data.gov.sk/set/rpi/gmd/42181810/4F21D34AE8FE7BA0E0530110000A5453","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329312CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329342CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329352CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329362CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F250933293E2CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329402CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F250933291D2CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F250933292A2CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F250933291E2CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F250933291D2CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329202CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/8A671134A5BE737AE0530210000A3952","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329282CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/69F4AAE259DC5CFBE0530110000A08B1","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329452CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329292CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F250933292B2CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F250933292F2CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329302CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F2D8FE3794142D2E0530210000AA214","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329412CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F25093329432CA2E0530210000A0ECD","https://data.gov.sk/set/rpi/gmd/42181810/4F2D8FE3794342D2E0530210000AA214","https://data.gov.sk/set/rpi/gmd/42181810/4F2D8FE3794442D2E0530210000AA214","https://data.gov.sk/set/rpi/gmd/42181810/4F2D8FE3794542D2E0530210000AA214"]

CSW_ENDPOINT = 'https://rpi.gov.sk/rpi_csw/service.svc/get'

csw = CatalogueServiceWeb(CSW_ENDPOINT)


def get_record_to_gmd(domXml):
    xslt = b'''<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="no"/>
    <!-- identity template -->
        <xsl:template match="node() | @*">
          <xsl:copy>
            <xsl:apply-templates select="node() | @*" />
          </xsl:copy>
        </xsl:template>

        <!-- template for the document element -->
        <xsl:template match="/*">
          <xsl:apply-templates select="node()" />
        </xsl:template>
        </xsl:stylesheet>
    '''
    xslt_doc = ET.parse(io.BytesIO(xslt))
    transform = ET.XSLT(xslt_doc)
    result = transform(domXml)
    return ET.tostring(result, pretty_print=True, encoding='utf-8')


def get_record_save_xml(file_identifier):
	file_name = file_identifier.replace("/","_").replace(".","_").replace(":","_")
	file_name = f"{file_name}.xml"

	identifier_query =  PropertyIsEqualTo('apiso:Identifier', file_identifier)
	with open(file_name,'w') as md_file:
		try:
			csw.getrecordbyid([file_identifier], outputschema='http://www.isotc211.org/2005/gmd')
		except:
			print(csw.response)
		else:
			gmd_record = get_record_to_gmd(ET.XML(csw.response))
			md_file.write(gmd_record.decode('utf-8'))

for record in _FILE_IDENTIFIERS:
	get_record_save_xml(record)